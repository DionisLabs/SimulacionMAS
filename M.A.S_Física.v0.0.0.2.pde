/*Autor: David Dionis Soler

Archivo con codificación universal
Charset: UTF-8
Revisión: v0.0.0.2 (fase pre-alpha)

*/

ControlP5 cp5;

float diametro=300;
float radio=diametro/2;
float posx=250;
float posy=100;
 //(rad/s)
float momento=0;
float y;
float x;
float centrarx=100;
float centrary=100;
PFont Fuente15;
PFont Fuente30;
PFont FuenteGP;
import controlP5.*;



Slider slider;

void setup() {
    size(1700,900);
    background(255,255,255);
    surface.setTitle("Simulación M.A.S || by DionisLabs");
    Fuente15 = createFont("Arial Bold", 15);
    Fuente30 = createFont("Arial Bold", 30);
    FuenteGP= createFont("Georgia Pro", 30);
    cp5 = new ControlP5(this);
    
    slider=cp5.addSlider("Slider").setSize(450,30).setPosition(100,760).setRange(0,10);
    slider.setValue(1);
    
}

void draw() {
    //Limpiar pantalla
    background(255,255,255);
    
    //Velocidad angular:
    float valslider=slider.getValue();
    float vel_angular=(valslider/100); //(rad/s)
    
    //Dibujar objetos y texto sin físicas:
    fill(0,0,0);
    textFont(Fuente30);
    text("Simulación del Movimiento Armónico Simple",25,55);
    strokeWeight(3);
    stroke(0,0,0);
    line(centrarx+500,centrary+135,centrarx+750,centrary+135);
    noFill();
    stroke(0,0,0);
    strokeWeight(4);
    ellipse(centrarx+100+radio, centrary+150+radio, diametro, diametro);
    fill(180,100,180);
    textFont(FuenteGP);
    text("Espacio Fásico",centrarx+135,centrary+100);
    fill(100,100,250);
    text("Espacio Real",centrarx+550,centrary+100);
    
    
    //Pendiente
    fill(100,240,150);
    text("Gráfica de la onda",centrarx+1100,centrary+100);
    fill(0,0,0);
    textFont(Fuente15);
    text("(Pendiente)",centrarx+1180,centrary+130);
    stroke(255,0,0);
    strokeWeight(4);
    line(1150,300,1500,650);
    stroke(255,0,0);
    strokeWeight(4);
    line(1500,300,1150,650);
    
    
    //Calculos posición Y en el espacio real y fásico
    momento+=2;
    y=300+(radio*sin(vel_angular*momento));  //-->Formula de la equación del movimiento armónico simple
    posy=500;
    
    //Calculos posición X en el espacio fásico:
    x=radio*-cos(vel_angular*momento);
    
    //Cuadrado muelle
    fill(0,255,0);
    noStroke();
    rect(centrarx+600, centrary+y, 50,50);
    strokeWeight(3);
    stroke(0,0,0);
    line(centrarx+625,centrary+135,centrarx+625,centrary+y);
    delay(50);
    
    //Punto del espacio fásico
    noStroke();
    fill(0,255,0);
    ellipse(centrarx+250+x, centrary+y, 20, 20);
    
    //Valores del vector:
    fill(0,0,0);
    textFont(FuenteGP);
    text("Valores:",100,670);
    fill(0,0,0);
    textFont(Fuente15);
    text("Valor X: "+(x)/radio,100,700);
    text("Valor Y (Amplitud): "+(((y)-300)*(-1))/radio,100,725);
    text("Velocidad Angular: "+valslider+" rad/s",100,750);
    
}