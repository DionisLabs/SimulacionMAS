# Descripción

Simple simulación de la relación entre espacio real y el espacio fásico/de fases realizado en Processing 3.4


- Simulación M.A.S. (Movimiento Armónico Simple):
$`y(t)=Asin(wt)`$.

- Simulación Ecuación de onda:
$`y(t)=Asin(wt-kw+\gamma ^{0})`$.
